# ✈️ Reto 3.1 DDL - Definición de datos en MySQL ✈️

## 🗂️ Estructura de la Base de Datos 🗂️

Para la creación de la BBDD habrá que crear 3 tablas principales:

* **vuelos**: guardará la información de los vuelos
    * ✈️ **ID del vuelo (id_vuelo)** [clave primaria]
    * 🌍 **Origen (origen)** {No puede ser nulo}
    * 🌍 **Destino (destino)** {No puede ser nulo}
    * 📅 **Fecha de salida (fecha_salida)** {No puede ser nulo}
    * 📅 **Fecha de llegada al destino (fecha_llegada)** {No puede ser nulo}
    * 🛩️ **Modelo del avión (avion)** {No puede ser nulo}

* **pasajeros**
    * 🆔 **ID del pasajero (id_pasajero)** [clave primaria]
    * 🧑‍✈️ **Nombre del pasajero (nombre)** {No puede ser nulo}
    * 🧑‍✈️ **Apellido del pasajero (apellido)** {No puede ser nulo}
    * 📇 **DNI del pasajero (dni)** {No puede ser nulo}

* **reservas**
    * 🆔 **ID de reserva (id_reserva)** [clave primaria]
    * ✈️ **ID del vuelo (id_vuelo)** [clave foránea] {No puede ser nulo}
    * 🧑‍✈️ **ID del pasajero (id_pasajero)** [clave foránea] {No puede ser nulo}
    * 💺 **Asiento (asiento)** {No puede ser nulo}



### Este script es un ejemplo de cómo generar unas tablas con la estructura de arriba:

```sql
CREATE TABLE vuelos (
    id_vuelo INT PRIMARY KEY AUTO_INCREMENT,
    origen VARCHAR(50) NOT NULL,
    destino VARCHAR(50) NOT NULL,
    fecha_salida DATETIME NOT NULL,
    fecha_llegada DATETIME NOT NULL,
    avion VARCHAR(50) NOT NULL
);

CREATE TABLE pasajeros (
    id_pasajero INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    apellido VARCHAR(100) NOT NULL,
    dni VARCHAR(20) UNIQUE NOT NULL
);

CREATE TABLE reservas (
    id_reserva INT PRIMARY KEY AUTO_INCREMENT,
    id_vuelo INT NOT NULL,
    id_pasajero INT NOT NULL,
    asiento VARCHAR(10) NOT NULL,
    FOREIGN KEY (id_vuelo) REFERENCES vuelos(id_vuelo),
    FOREIGN KEY (id_pasajero) REFERENCES pasajeros(id_pasajero),
    UNIQUE (id_vuelo, asiento)
);

```
### 🛡️ Restricciones 
##### 🔑 UNIQUE
En el siguiente ejemplo, esta restricción nos evita que se asigne el mismo asiento para dos pasajeros el mismo vuelo
```sql
UNIQUE (id_vuelo, asiento)
```
##### 🔗 FOREIGN KEY
En el próximo ejemplo se ve como se relaciona (id_vuelo) de la tabla vuelos y la (id_vuelo) de la tabla reservas, lo que asegura que cada nueva reserva tenga asociada una ID de vuelo
```sql
FOREIGN KEY (id_vuelo) REFERENCES vuelos(id_vuelo)
```
##### 🚫 NOT NULL
Esta restricción es fundamental para que todos los datos se rellenen correctamente, impidiendo se queden espacios en blanco en las tablas. Aquí un ejemplo:
```sql
dni VARCHAR(20) UNIQUE NOT NULL
```
##### 🆔 CLAVE PRIMARIA (PRIMARY KEY)
La clave primaria idéntifica de forma única cada fila de una tabla, aquí un ejemplo de la base de datos de arriba:
```sql
id_pasajero INT PRIMARY KEY AUTO_INCREMENT
```

#### 📜 OTRAS RESTRICCIONES
* **Restricciones de datos**
    * 🔢 **INT**: restringe la inserción de datos que no sean números
    ```sql
    id_vuelo INT NOT NULL
    ```
    * 🔤 **VARCHAR**: sirve para almacenar cadena de caracteres hasta un límite fijado
    ```sql
    asiento VARCHAR(10) NOT NULL
    ```
    * 📅 **DATETIME**: se usa para almacenar fechas
    ```sql
    fecha_salida DATETIME NOT NULL
    ```

## 🖥️ Explorar y Utilizar la Base de Datos Mediante CLI 🖥️
Hay una lista de comandos básicos que son esenciales para poder utilizar y consultar la base de datos:
* 🔍 **SHOW_DATABASES**: Muestra todas las bases de datos existentes en el servidor
* 📂 **USE**: Selecciona una base de datos en concreto para trabajar con ella
* 📄 **SHOW TABLES**: Muestra todas las tablas de la base de datos seleccionada
* 📊 **SHOW COLUMN FROM (*inserte tabla especifica)**: Mediante este comando podemos ver la disposición que tiene una tabla en especifico

## 💼 Gestión de transacciones 💼
Para gestionar las transacciones de datos, se usan principalmente los siguientes tres comandos:
* 💾 **COMMIT**: Este comando se usa para confirmar los cambios realizados en una transacción, es decir, todos los cambios que preceden al `COMMIT` serán permanentes
* 🔄 **AUTOCOMMIT**: Mediante este comando todas las sentencias SQL que hagamos se convertirán permanentes sin necesidad de `COMMIT`, siempre y cuando se haya activado
* ⏪ **ROLLBACK**: Este último comando lo que hace es deshacer todos los cambios que no se hayan confirmado con un `COMMIT`, en caso que se haya activado el `AUTOCOMMIT` este comando será inutil
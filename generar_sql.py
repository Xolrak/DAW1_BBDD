import os

def generar_archivos_sql():
    num_archivos = int(input("¿Cuántos archivos SQL quieres generar? "))
    anotacion = "-- Carlos C de DAW1 ^^"

    # Verificar archivos existentes
    archivos_existentes = [f for f in os.listdir() if f.startswith("query") and f.endswith(".sql")]
    ultimo_numero = 0
    if archivos_existentes:
        ultimo_numero = max([int(f[5:-4]) for f in archivos_existentes])

    # Generar nuevos archivos
    for i in range(ultimo_numero + 1, ultimo_numero + num_archivos + 1):
        nombre_archivo = f"query{i:02d}.sql"  # Formatear con dos dígitos (01, 02...)
        if not os.path.exists(nombre_archivo):
            with open(nombre_archivo, "w") as f:
                f.write(anotacion + "\n")
            print(f"Archivo creado: {nombre_archivo}")
        else:
            print(f"Archivo ya existente: {nombre_archivo}")

if __name__ == "__main__":
    generar_archivos_sql()

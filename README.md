# <span style="color:blue">Repositorio de Base de Datos de Carlos</span>

Este repositorio contiene todo las tareas hechas en la Base de Datos.

## Concepto y origen de las bases de datos

Las BBDD es un tipo de software que se encarga de organizar y administrar datos en un formato estructurado. Esto consigue que se guarden de manera eficiente además de poder manipularlos.
El origen se remonta a los años 60, cuando los ordenadores se comenzaron a usar para gestionar gran cantidad de datos. Fue en la decada de los 70 cuando las bases de datos se empezaron a popularizar y en 1980 fue cuando se desarrollaron las primeras bases de datos SQL.
Las principales funciones de las bases de datos son:
- Almacenar eficientemente grandes cantidades de informacion
- Organizar y gestionar informacion
- Acceso rapido y eficiente a la informacion
- Seguridad y proteccion de la informacion almacenada
- Compartir informacion entre otros usuarios

## Sistemas de gestión de bases de datos

Son programas que permiten crear, administrar y acceder a una base de datos. Las principales caracteristicas que tienen son:

* Consultas
* Modificacion de datos
* Seguridad de datos
* Concurrencia de datos
* Recuperacion
* Escalabilidad de datos
* Integracion de datos
* Usabilidad de los datos

### Ejemplos de sistemas de gestión de bases de datos

* Oracle DB
* IMB Db2
* SQLite
* MariaDB
* SQL Server
* PostgreSQL
* mySQL

## Modelo cliente-servidor

Tener el DBMS en un servidor ofrece muchas ventajas, como la centralización, la seguridad, la escalabilidad, el rendimiento y la facilidad de uso. 
Desacoplar al DBMS del cliente también ofrece muchas ventajas, como la flexibilidad, el mantenimiento, la seguridad y la escalabilidad.

## SQL

SQL que significa (Structured Query Language) es un lenguaje de programación fundamental para trabajar con bases de datos relacionales. Es un lenguaje versátil, potente y fácil de aprender.

### Instrucciones de SQL

#### DDL (Lenguaje de Definición de Datos)
#### DML (Lenguaje de Manipulacion de Datos)
#### DCL (Lenguaje de Control de Datos)
#### TCL (Lenguaje de Control de Transacciones)

## Bases de datos relacionales

Las bases de datos relacionales son un tipo de base de datos muy popular debido a su simplicidad, flexibilidad, escalabilidad, integridad y seguridad.
Estas bases de datos son la opcion predominante para muchas organizaciones debido a su solida combinacion de organizacion de datos, capacidad de consulta, integridad de datos y escalabilidad.

* __Relación (tabla)__: Sirve como contenedor para los datos, similar a Excel
* __Atributo/Campo (columna)__: Caracteristicas individuales de las entidades almacenadas en una tabla
* __Registro (fila)__: Cada fila en una tabla captura una instancia especifica de la entidad que representa la tabla, junto con los valores de sus atributos (las columnas)

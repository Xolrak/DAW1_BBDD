-- Carlos C de DAW1 ^^
-- Muestra el importe medio, mínimo y máximo de cada factura.

SELECT 
    Invoice.InvoiceId AS FacturaId, -- Seleccionamos el ID de la factura
    AVG(Invoice.Total) AS ImporteMedio, -- Calculamos el importe medio de cada factura y lo mostramos como "ImporteMedio"
    MIN(Invoice.Total) AS ImporteMinimo, -- Calculamos el importe mínimo de cada factura y lo mostramos como "ImporteMinimo"
    MAX(Invoice.Total) AS ImporteMaximo -- Calculamos el importe máximo de cada factura y lo mostramos como "ImporteMaximo"
FROM 
    Invoice -- Desde la tabla Invoice
GROUP BY 
    Invoice.InvoiceId; -- Agrupamos los resultados por ID de factura
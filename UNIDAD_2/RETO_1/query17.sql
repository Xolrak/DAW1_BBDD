-- Carlos C de DAW1 ^^
-- Muestra los álbumes ordenados por el número de canciones que tiene cada uno.

SELECT 
    Album.Title AS TituloAlbum, -- Seleccionamos el título del álbum y lo aliasamos como "TituloAlbum"
    COUNT(Track.TrackId) AS NumeroCanciones -- Contamos el número de canciones asociadas a cada álbum y lo mostramos como "NumeroCanciones"
FROM 
    Album -- Desde la tabla Album
LEFT JOIN 
    Track ON Album.AlbumId = Track.AlbumId -- Hacemos un left join con la tabla Track usando AlbumId para obtener información de las canciones asociadas a cada álbum
GROUP BY 
    Album.Title -- Agrupamos los resultados por título del álbum
ORDER BY 
    COUNT(Track.TrackId) DESC; -- Ordenamos los resultados por el número de canciones de forma descendente

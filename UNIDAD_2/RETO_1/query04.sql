-- Carlos C de DAW1 ^^
-- Muestra las 10 canciones que más tamaño ocupan.

-- selecciono todo de la base de datos Chinook
SELECT *
-- dentro de ella me posiciono en Track (cancion)
FROM Chinook.Track
-- ordeno por bytes (el tamaño de las canciones) en orden descendente
ORDER BY Bytes DESC
-- mediante "LIMIT" limito el orden a las 10 canciones mas pesadas
LIMIT 10;

-- Carlos C de DAW1 ^^
-- Encuentra los géneros musicales más populares (los más comprados).

SELECT 
    Genre.Name AS Genero, -- Seleccionamos el nombre del género y lo aliasamos como "Genero"
    COUNT(*) AS TotalCompras -- Contamos el número total de compras asociadas a cada género y lo mostramos como "TotalCompras"
FROM 
    Genre -- Desde la tabla Genre
JOIN 
    Track ON Genre.GenreId = Track.GenreId -- Unimos con la tabla Track usando GenreId para obtener información de las canciones asociadas a cada género
JOIN 
    InvoiceLine ON Track.TrackId = InvoiceLine.TrackId -- Unimos con la tabla InvoiceLine usando TrackId para obtener información de las compras asociadas a cada canción
GROUP BY 
    Genre.Name -- Agrupamos los resultados por nombre de género
ORDER BY 
    TotalCompras DESC; -- Ordenamos los resultados por el total de compras de forma descendente

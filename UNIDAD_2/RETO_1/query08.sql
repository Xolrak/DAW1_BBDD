-- Carlos C de DAW1 ^^ç
-- Muestra los nombres de los 15 empleados más jóvenes 
-- junto a los nombres de sus supervisores, si los tienen.

-- selecciono el ID, el apellido y el nombre de los empleados
SELECT 
    E1.EmployeeId AS "ID Empleado", -- Seleccionamos el ID del primer empleado y lo aliasamos como "ID Empleado"
    E1.LastName AS "Apellido Empleado", -- Seleccionamos el apellido del primer empleado y lo aliasamos como "Apellido Empleado"
    E1.FirstName AS "Nombre Empleado", -- Seleccionamos el nombre del primer empleado y lo aliasamos como "Nombre Empleado"
    E2.EmployeeId AS "ID Empleado", -- Seleccionamos el ID del segundo empleado y lo aliasamos como "ID Empleado"
    E2.LastName AS "Apellido Empleado", -- Seleccionamos el apellido del segundo empleado y lo aliasamos como "Apellido Empleado"
    E2.FirstName AS "Nombre Empleado", -- Seleccionamos el nombre del segundo empleado y lo aliasamos como "Nombre Empleado"
    CONCAT(E1.LastName,", ", E1.FirstName), -- Concatenamos el apellido y el nombre del primer empleado para mostrarlo juntos
    CONCAT(E2.LastName,", ", E2.FirstName) -- Concatenamos el apellido y el nombre del segundo empleado para mostrarlo juntos
-- Desde la tabla Employee (empleados), realizamos una auto-asociación para comparar empleados con sus supervisores
FROM Employee AS E1
JOIN Employee AS E2
ON E1.ReportsTo = E2.EmployeeId -- Unimos las filas donde el empleado 1 informa al empleado 2
-- Ordenamos los resultados por fecha de cumpleaños en orden descendente
-- y limitamos la salida a 5 empleados
ORDER BY E1.BirthDate DESC
LIMIT 5;
-- Carlos C de DAW1 ^^
-- Muestra qué clientes han realizado compras por valores superiores a 10€, ordenados por apellido.

SELECT 
    Customer.FirstName AS Nombre, -- Seleccionamos el primer nombre del cliente y lo aliasamos como "Nombre"
    Customer.LastName AS Apellido, -- Seleccionamos el apellido del cliente y lo aliasamos como "Apellido"
    Invoice.Total AS Importe -- Seleccionamos el importe total de la factura y lo aliasamos como "Importe"
FROM 
    Customer -- Desde la tabla Customer
JOIN 
    Invoice ON Customer.CustomerId = Invoice.CustomerId -- Unimos con la tabla Invoice usando CustomerId para obtener información de las facturas asociadas a los clientes
WHERE 
    Invoice.Total > 10 -- Filtramos las facturas por un importe total mayor a 10
ORDER BY 
    Customer.LastName; -- Ordenamos los resultados por apellido del cliente
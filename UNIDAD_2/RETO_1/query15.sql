-- Carlos C de DAW1 ^^
-- Muestra el número de países donde tenemos clientes.

SELECT 
    COUNT(DISTINCT Country) AS NumeroPaises -- Contamos el número de países únicos y lo mostramos como "NumeroPaises"
FROM 
    Customer; -- Desde la tabla Customer

-- Carlos C de DAW1 ^^
-- Lista los 6 álbumes que acumulan más compras.

SELECT 
    Album.Title AS TituloAlbum, -- Seleccionamos el título del álbum y lo aliasamos como "TituloAlbum"
    COUNT(InvoiceLine.InvoiceLineId) AS TotalCompras -- Contamos el número total de compras asociadas a cada álbum y lo mostramos como "TotalCompras"
FROM 
    Album -- Desde la tabla Album
JOIN 
    Track ON Album.AlbumId = Track.AlbumId -- Unimos con la tabla Track usando AlbumId para obtener información de las canciones asociadas a cada álbum
JOIN 
    InvoiceLine ON Track.TrackId = InvoiceLine.TrackId -- Unimos con la tabla InvoiceLine usando TrackId para obtener información de las compras asociadas a cada canción
GROUP BY 
    Album.Title -- Agrupamos los resultados por título del álbum
ORDER BY 
    TotalCompras DESC -- Ordenamos los resultados por el total de compras de forma descendente
LIMIT 6; -- Limitamos los resultados a los 6 álbumes con más compras
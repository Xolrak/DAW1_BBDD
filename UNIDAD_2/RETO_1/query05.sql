-- Carlos C de DAW1 ^^
-- Muestra el nombre de aquellos países en los que tenemos clientes.

-- selecciono solo Country (paises) de la base de datos Chinook
-- "DISTINCT" hace que no se repitan los paises que aparecen que puedan haber varios clientes
SELECT DISTINCT Country
-- dentro de ella me posiciono en Customer (clientes)
FROM Chinook.Customer;

-- Carlos C de DAW1 ^^
-- Muestra el número de canciones de cada género (deberá mostrarse el nombre del género).

SELECT 
    Genre.Name AS Genero, -- Seleccionamos el nombre del género y lo aliasamos como "Genero"
    COUNT(Track.TrackId) AS NumeroCanciones -- Contamos el número de canciones asociadas a cada género y lo mostramos como "NumeroCanciones"
FROM 
    Genre -- Desde la tabla Genre
JOIN 
    Track ON Genre.GenreId = Track.GenreId -- Unimos con la tabla Track usando GenreId para obtener información de las canciones asociadas a cada género
GROUP BY 
    Genre.Name; -- Agrupamos los resultados por nombre de género
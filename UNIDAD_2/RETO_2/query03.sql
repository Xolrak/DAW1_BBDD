-- Carlos C de DAW1 ^^
-- Muestra nombre y apellidos de los empleados que tienen clientes asignados.
-- (Pista: se puede hacer con EXISTS).
SELECT 
    e.FirstName, -- Seleccionamos el primer nombre del empleado
    e.LastName -- Seleccionamos el apellido del empleado
FROM 
    Employee e -- Desde la tabla Employee, la aliasamos como "e"
WHERE 
    EXISTS (
        -- Subconsulta: Verificamos si existe al menos un cliente asignado a este empleado
        SELECT 
            1 -- No importa qué columna seleccionamos, solo necesitamos verificar la existencia de al menos una fila
        FROM 
            Customer c -- Desde la tabla Customer, la aliasamos como "c"
        WHERE 
            c.SupportRepId = e.EmployeeId -- Comparamos el ID del representante de soporte del cliente con el ID del empleado
    );
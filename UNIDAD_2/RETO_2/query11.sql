-- Carlos C de DAW1 ^^
-- Muestra las canciones con una duración superior a la media.

SELECT 
    Name AS NombreCancion, -- Selecciona el nombre de la canción
    Milliseconds AS Duracion -- Selecciona la duración de la canción
FROM 
    Track -- Selecciona de la tabla "Track"
WHERE 
    Milliseconds > ( -- Compara la duración con la duración media
        SELECT 
            AVG(Milliseconds) -- Calcula la duración media
        FROM 
            Track -- La subconsulta se realiza en la misma tabla "Track"
    );

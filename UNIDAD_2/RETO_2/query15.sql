-- Carlos C de DAW1 ^^
-- Calcula el descuento aplicado sobre cada factura, que depende del monto total
-- de la factura (10% para facturas superiores a $100).

SELECT 
    InvoiceId, -- Selecciona el ID de la factura
    Total, -- Selecciona el monto total de la factura
    CASE -- Utiliza una expresión CASE para aplicar el descuento si es necesario
        WHEN Total > 100 THEN Total * 0.1 -- Aplica un descuento del 10% si el monto total de la factura es superior a $100
        ELSE 0 -- No aplica descuento si el monto total de la factura es igual o inferior a $100
    END AS Descuento -- El resultado del descuento se nombra como "Descuento"
FROM 
    (
        SELECT 
            InvoiceId, -- Selecciona el ID de la factura
            Total -- Selecciona el monto total de la factura
        FROM 
            Invoice -- Selecciona de la tabla "Invoice"
    ) AS InvoiceData; -- Utiliza una subconsulta para obtener los datos de las facturas y la aliasa como "InvoiceData"

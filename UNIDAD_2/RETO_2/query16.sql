-- Carlos C de DAW1 ^^
-- Clasifica a los empleados en diferentes niveles según su cargo (manager, asistente o empleado).

SELECT 
    EmployeeId, -- Selecciona el ID del empleado
    FirstName, -- Selecciona el primer nombre del empleado
    LastName, -- Selecciona el apellido del empleado
    Title, -- Selecciona el título del empleado
    CASE -- Utiliza una expresión CASE para clasificar a los empleados según su título
        WHEN Title = 'General Manager' THEN 'Manager' -- Clasifica como "Manager" si el título es "General Manager"
        WHEN Title LIKE '%Assistant%' THEN 'Asistente' -- Clasifica como "Asistente" si el título contiene "Assistant"
        ELSE 'Empleado' -- Clasifica como "Empleado" para cualquier otro título
    END AS Nivel -- El resultado de la clasificación se nombra como "Nivel"
FROM 
    Employee; -- Selecciona de la tabla "Employee"

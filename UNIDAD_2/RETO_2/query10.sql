-- Carlos C de DAW1 ^^
-- Muestra las 10 canciones más compradas.

SELECT 
    T.Name AS NombreCancion, -- Selecciona el nombre de la canción y lo nombra como "NombreCancion"
    (
        -- Subconsulta para contar cuántas veces ha sido comprada la canción
        SELECT 
            COUNT(*) -- Contamos las filas en la tabla InvoiceLine donde el TrackId coincide con el de la canción actual
        FROM 
            InvoiceLine AS IL -- Desde la tabla InvoiceLine, la aliasamos como "IL"
        WHERE 
            IL.TrackId = T.TrackId -- Filtramos las filas donde el TrackId coincide con el de la canción actual
    ) AS VecesComprada -- Utiliza una subconsulta para contar cuántas veces ha sido comprada la canción y lo nombra como "VecesComprada"
FROM 
    Track AS T -- Alias de la tabla "Track" como "T"
ORDER BY 
    VecesComprada DESC -- Ordena los resultados en orden descendente según la cantidad de veces que ha sido comprada la canción
LIMIT 
    10; -- Limita los resultados a las 10 canciones más compradas

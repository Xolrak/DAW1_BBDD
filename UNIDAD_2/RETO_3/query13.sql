-- Carlos C de DAW1 ^^
-- Álbumes junto al número de canciones en cada uno.

SELECT
    a.AlbumId, -- Seleccionamos el ID del álbum
    a.Title AS TituloAlbum, -- Seleccionamos el título del álbum
    COUNT(t.TrackId) AS NumeroCanciones -- Contamos el número de canciones en cada álbum
FROM
    Album AS a -- Desde la tabla Album
LEFT JOIN
    Track AS t ON a.AlbumId = t.AlbumId -- Unimos con la tabla Track basándonos en el ID del álbum
GROUP BY
    a.AlbumId, -- Agrupamos por el ID del álbum
    a.Title; -- Agrupamos por el título del álbum

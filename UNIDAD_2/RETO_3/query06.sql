-- Carlos C de DAW1 ^^
-- Obtener los álbumes con un número de canciones superiores a la media.

SELECT 
    A.Title AS AlbumTitulo, -- Selecciona el título del álbum
    AR.Name AS ArtistaNombre, -- Selecciona el nombre del artista
    SongCount.NumCanciones -- Selecciona el número de canciones del álbum
FROM 
    Album AS A -- Desde la tabla Album
JOIN 
    Artist AS AR ON A.ArtistId = AR.ArtistId -- Une con la tabla Artist usando ArtistId
JOIN 
    (
        SELECT 
            T.AlbumId, -- Selecciona el AlbumId
            COUNT(T.TrackId) AS NumCanciones -- Cuenta el número de canciones por álbum
        FROM 
            Track AS T -- Desde la tabla Track
        GROUP BY 
            T.AlbumId -- Agrupa por AlbumId
    ) AS SongCount ON A.AlbumId = SongCount.AlbumId -- Une la subconsulta con la tabla Album usando AlbumId
WHERE 
    SongCount.NumCanciones > (
        SELECT 
            AVG(SongCounts.NumCanciones) -- Calcula la cantidad media de canciones por álbum
        FROM 
            (
                SELECT 
                    COUNT(T.TrackId) AS NumCanciones -- Cuenta el número de canciones por álbum
                FROM 
                    Track AS T -- Desde la tabla Track
                GROUP BY 
                    T.AlbumId -- Agrupa por AlbumId
            ) AS SongCounts
    );

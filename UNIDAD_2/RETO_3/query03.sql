-- Carlos C de DAW1 ^^
-- Mostrar las listas de reproducción en las que hay canciones de reggae

SELECT 
    Playlist.Name AS NombreListaReproduccion -- Selecciona el nombre de las listas de reproducción
FROM 
    Playlist -- Desde la tabla Playlist
WHERE 
    PlaylistId IN ( -- Filtra por los PlaylistId que están en la siguiente subconsulta
        SELECT 
            PlaylistId -- Selecciona los PlaylistId
        FROM 
            PlaylistTrack -- Desde la tabla PlaylistTrack
        WHERE 
            TrackId IN ( -- Filtra por los TrackId que están en la siguiente subconsulta
                SELECT 
                    TrackId -- Selecciona los TrackId
                FROM 
                    Track -- Desde la tabla Track
                WHERE 
                    GenreId = ( -- Filtra por el GenreId que corresponde a "Reggae"
                        SELECT 
                            GenreId -- Selecciona el GenreId
                        FROM 
                            Genre -- Desde la tabla Genre
                        WHERE 
                            Name = 'Reggae' -- Donde el nombre del género es 'Reggae'
                    )
            )
    );

-- Carlos C de DAW1 ^^
-- Obtener las canciones con una duración superior a la media.

SELECT 
    Name AS NombreCancion, -- Selecciona el nombre de la canción
    Milliseconds AS Duracion -- Selecciona la duración de la canción
FROM 
    Track -- Selecciona de la tabla "Track"
WHERE 
    Milliseconds > (
        SELECT 
            AVG(Milliseconds) -- Calcula la duración media
        FROM 
            Track
    );

-- Carlos C de DAW1 ^^
-- Obtener el nombre del álbum más reciente de cada artista. 
-- Pista: los álbumes más antiguos tienen un ID más bajo.

SELECT
    ar.ArtistId, -- Seleccionamos el ID del artista
    ar.Name AS NombreArtista, -- Seleccionamos el nombre del artista
    al.AlbumId AS AlbumRecienteId, -- Seleccionamos el ID del álbum más reciente
    al.Title AS AlbumReciente -- Seleccionamos el título del álbum más reciente
FROM
    Album AS al -- Desde la tabla Album
JOIN
    Artist AS ar ON al.ArtistId = ar.ArtistId -- Unimos con la tabla Artist usando ArtistId
JOIN
    (
        -- Subconsulta para obtener el ID del álbum más reciente para cada artista
        SELECT
            ArtistId,
            MAX(AlbumId) AS MaxAlbumId -- Seleccionamos el ID máximo del álbum
        FROM
            Album
        GROUP BY
            ArtistId -- Agrupamos por ArtistId para obtener el máximo para cada artista
    ) AS a ON al.ArtistId = a.ArtistId AND al.AlbumId = a.MaxAlbumId; -- Unimos la subconsulta con las tablas Album y Artist

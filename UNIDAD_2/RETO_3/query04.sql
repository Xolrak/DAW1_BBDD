-- Carlos C de DAW1 ^^
-- Obtener la información de los clientes que han realizado compras superiores a 20€.

SELECT 
    Customer.CustomerId, -- Selecciona el ID del cliente
    Customer.FirstName, -- Selecciona el nombre del cliente
    Customer.LastName, -- Selecciona el apellido del cliente
    Customer.Email, -- Selecciona el email del cliente
    Customer.Phone -- Selecciona el teléfono del cliente
FROM 
    Customer -- Desde la tabla Customer
WHERE 
    Customer.CustomerId IN ( -- Filtra por los CustomerId que están en la siguiente subconsulta
        SELECT 
            CustomerId -- Selecciona los CustomerId
        FROM 
            Invoice -- Desde la tabla Invoice
        WHERE 
            Total > 20 -- Donde el total de la factura es superior a 20€
    );

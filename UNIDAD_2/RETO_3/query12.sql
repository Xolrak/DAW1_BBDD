-- Carlos C de DAW1 ^^
-- Ventas totales de cada empleado.

SELECT
    e.EmployeeId, -- Seleccionamos el ID del empleado
    e.FirstName, -- Seleccionamos el nombre del empleado
    e.LastName, -- Seleccionamos el apellido del empleado
    SUM(i.Total) AS VentasTotales -- Sumamos el total de todas las ventas para cada empleado
FROM
    Employee AS e -- Desde la tabla Employee
LEFT JOIN
    Customer AS c ON e.EmployeeId = c.SupportRepId -- Unimos con la tabla Customer usando SupportRepId
LEFT JOIN
    Invoice AS i ON c.CustomerId = i.CustomerId -- Unimos con la tabla Invoice usando CustomerId
GROUP BY
    e.EmployeeId, -- Agrupamos por el ID del empleado
    e.FirstName, -- Agrupamos por el nombre del empleado
    e.LastName; -- Agrupamos por el apellido del empleado
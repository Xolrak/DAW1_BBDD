# SCRIPTS

## SCRIPT DE PYTHON PARA LA CARGA Y DESCARGA DEL REPOSITORIO A GITLAB

Este script se encarga de cargar y descargar el repositorio independientemente del sistema operativo en el que se ejecute, es compatible tanto con Windows como con Linux


## SCRIPT COMPILADOR DE .PY A .PYC

Sirve para compilar el script de Carga y Descarga y ejecutarlo con doble click en Windows


## SCRIPT DE DOCKER

Dentro de docker.zip hay 3 archivos necesarios para crear la maquina virtual en Docker.
-- seleccion de columnas que queremos ver dentro de la tabla
SELECT *
-- tabla a la que accedo
FROM ACTOR
-- filtro las filas de una tabla en funcion de una condicion especifica
-- en este caso el LIKE
WHERE Nom
-- Busco con el LIKE dentro de la filas de la tabla la palabra puesta
-- en funcion de donde esté el '%' si va al principio buscará sufijos
-- si va al final el '%' buscará prefijos
-- si va intercalado el '%' buscará palabras con las letras puestas
LIKE 'X%'
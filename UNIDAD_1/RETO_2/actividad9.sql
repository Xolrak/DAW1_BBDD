-- seleccion de columnas que queremos ver dentro de la tabla
SELECT *
-- tabla a la que accedo
FROM DETALLFACTURA
-- selecciono las filas de la tabla y las ordeno en orden descendente
ORDER BY PreuUnitari DESC;
-- seleccion de columnas que queremos ver dentro de la tabla
SELECT CLIENT_COD, NOM, CIUTAT,AREA,TELEFON 
-- tabla a la que accedo
FROM CLIENT
-- Filtro las filas donde los primeros 3 caracteres de TELEFON no son '636'
WHERE SUBSTRING(TELEFON,1,3) != '636';
SELECT P.Titol, A.Nom
FROM PELICULA P
JOIN INTERPRETADA I ON P.CodiPeli = I.CodiPeli
JOIN ACTOR A ON I.CodiActor = A.CodiActor;

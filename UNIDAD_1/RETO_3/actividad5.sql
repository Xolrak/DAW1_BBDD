SELECT P1.CodiPeli, P1.Titol, P2.CodiPeli, P2.Titol
FROM PELICULA P1
LEFT JOIN PELICULA P2 ON P1.SegonaPart = P2.CodiPeli;
# <span style="color:blue">Consultas a la bbdd instagram_low_cost</span>
#### <span style="color:green">Carlos C de DAW1 ^^</span> 

## <span style="color:red">Query01: Fotos del usuario con ID 36</span>

```sql
-- selecciono todo dentro de fotos
SELECT *
FROM fotos
-- me situo dentro de idUsuario para ver las fotos del usuario 36
WHERE idUsuario = 36;
```
## <span style="color:red">Query02: Fotos del usuario con ID 36 tomadas en enero del 2023</span>

```sql
SELECT *
FROM fotos
WHERE idUsuario = 36
AND fechaCreacion BETWEEN '2023-01-01' AND '2024-02-01';
```
## <span style="color:red">Query03: Comentarios del usuario 36 sobre la foto 12 del usuario 11</span>

```sql

```
## <span style="color:red">Query04: Fotos que han sorprendido al usuario 25</span>

```sql
-- sin terminar --
SELECT *
FROM fotos
WHERE idFoto IN (SELECT idFoto FROM reaccionesFotos WHERE idUsuario = 25 AND idTipoReaccion = 4);
```
## <span style="color:red">Query05: Administradores que han dado más de  2 “Me gusta”</span>

```sql

```
## <span style="color:red">Query06: Número de “Me divierte” de la foto número 12 del usuario 45</span>

```sql

```
## <span style="color:red">Query07: Número de fotos tomadas en la playa (en base al título)</span>

```sql

```